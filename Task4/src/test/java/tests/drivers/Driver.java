package tests.drivers;


import org.openqa.selenium.WebDriver;
import tests.YandexTest;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.util.concurrent.TimeUnit;

public class Driver {
    private enum DriverCreationType {
        LocalChromeDriver,
        LocalFirefoxDriver,
        RemoteChromeDriver,
        RemoteFirefoxDriver,
    }
    private static DriverCreationType defaultDriverType = DriverCreationType.LocalChromeDriver;
    private static final String LOCAL_HOST = "http://localhost:4444/wd/hub";
    private static final String CHROME_DRIVER_PATH = "./MyWebDriver/chromedriver.exe";
    private static final String GECKO_DRIVER_PATH = "./MyWebDriver/geckodriver.exe";

    private static WebDriver getWebDriverInstance(DriverCreationType driverChoice) throws Exception {
        WebDriver driver = null;
        DriverCreator driverCreator = null;
        switch (driverChoice) {
            case LocalChromeDriver:
                driverCreator = new LocalChromeDriverCreator(CHROME_DRIVER_PATH);
                break;
            case RemoteChromeDriver:
                driverCreator = new RemoteChromeDriverCreator(LOCAL_HOST);
                break;
            case RemoteFirefoxDriver:
                driverCreator = new RemoteFireFoxDriverCreator(LOCAL_HOST);
                break;
            case LocalFirefoxDriver:
                driverCreator = new LocalFireFoxDriverCreator(GECKO_DRIVER_PATH);
                break;
            default:
                System.out.println("Choose valid driver");
        }

        try {
            driver = driverCreator.createDriver();
            addImplicitly(driver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return driver;
    }


    public static WebDriver getWebDriverInstance() throws Exception {
        return getWebDriverInstance(defaultDriverType);
    }


    private static void addImplicitly(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        // Maximize browser window
        driver.manage().window().maximize();
    }


}
